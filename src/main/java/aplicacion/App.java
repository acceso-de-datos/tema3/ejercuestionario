package aplicacion;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

//import java.util.HashMap;
//import java.util.Map;
//import org.postgresql.ds.PGSimpleDataSource;
import org.postgresql.util.PGobject;

import javax.persistence.EntityManager;

public class App {

	public static void main(String[] args) throws SQLException {
		Connection con = ConexionBDOR.getConexion();
		EntityManager em = ConexionBDOO.getConexion();

		try {
			Migrar.allMigrate(con, em);
		} catch(SQLException e) {
			System.out.println("Error en la migracion de datos!!!");
			System.out.println(e.getMessage());

		}
		ConexionBDOR.cerrar();
		ConexionBDOO.cerrar();
	}
}
// DOCUMENTACION postgresql           
//            Arrays: 
//            https://www.postgresql.org/docs/12/arrays.html
//            Tipos compuestos (composite-types) 
//            https://www.postgresql.org/docs/12/rowtypes.html 
//            Herencia 
//            https://www.postgresql.org/docs/12/static/ddl-inherit.html 
//         

// 	ESTRUCTURA del schema 'bdor' : CREATE SCHEMA bdor;
//            CREATE TYPE tlista_telefonos AS
//            (
//                prefijo char(4),
//                telefonos char(9) ARRAY
//            );
//            CREATE TYPE tdireccion AS
//            (
//                calle character varying(60),
//                numero smallint,
//                localidad character varying(30),
//                cpostal char(5),
//                pais varchar(30)
//            );
//            CREATE TABLE cliente
//            (
//                cod bigserial PRIMARY KEY,
//                nombre character varying(20),
//                apellidos varchar(40),
//                direccion tdireccion,
//                telefonos tlista_telefonos,
//                emails varchar(50) ARRAY           
//            );       
//           INSERT INTO cliente (nombre, apellidos, direccion, telefonos, emails) 
//            VALUES ('Richard','Matthew Stallman', ROW('Serreta',10, 'Alcoi', '03430', 'España'), 
//            ROW('0034',ARRAY['111111111','222222222','333333333']), 
//            ARRAY['email1@ad.es', 'email2@ad.es'] 
//           );
//         
//
// EJERCICIOS
//        // Ejercicio1
//        // Realiza una consulta sobre la tabla CLIENTE (creada anteriormente) que muestre el nombre, 
//        // los apellidos, la calle y la localidad de todos los clientes.
//        
//        // Ejercicio2
//        // Realiza una consulta sobre la tabla CLIENTE (creada anteriormente) que muestre el nombre, los apellidos,  
//        // el prefijo y el primer telefono de la lista de telefonos.
//        
//        // Ejerccio3
//        // Realiza una actualización en la dirección del cliente Richard, Concretamente en su localidad. 
//        // Ahora deberá ser Valencia.
//        
//        // Ejercicio4
//        // Realiza una actualización en los telefonos del cliente Richard. 
//        // Debemos cambiar el prefijo a 0035 y también su primer y segundo telefono a 123123123 y 321321321 respectivamente.
//    }
//}
