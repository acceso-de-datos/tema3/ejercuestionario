package aplicacion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.postgresql.ds.PGSimpleDataSource;

// Patrón singleton
public class ConexionBDOR {
	private static String IP = "192.168.1.42";
	private static final String JDBC_URL = "jdbc:postgresql://" + IP + ":5432/postgres?currentSchema=pruebas";

	private static Connection con = null;

	public static Connection getConexion() throws SQLException {
		if (con == null) {
//			Properties pc = new Properties();
//			pc.put("user", "batoi");
//			pc.put("password", "1234");
//			con = DriverManager.getConnection(JDBC_URL, pc);

			PGSimpleDataSource ds = new PGSimpleDataSource();
			ds.setUrl(JDBC_URL);
			ds.setUser("batoi");
			ds.setPassword("1234");
//			ds.setServerNames(new String[] { IP });
//			ds.setCurrentSchema("bdor");
			con = ds.getConnection();
		}
		return con;
	}

	public static void cerrar() throws SQLException {
		if (con != null) {
			con.close();
		}
	}

}