package aplicacion;

import javax.persistence.Embeddable;

@Embeddable
public class TTarjeta {
    private int numero_de_tarjeta;
    private String titular;
    private int mes;
    private String año_caducidad;
    private String tipo;
    private int cvc;

    public TTarjeta(int numero_de_tarjeta, String titular, int mes, String año_caducidad, String tipo, int cvc) {
        this.numero_de_tarjeta = numero_de_tarjeta;
        this.titular = titular;
        this.mes = mes;
        this.año_caducidad = año_caducidad;
        this.tipo = tipo;
        this.cvc = cvc;
    }

    public int getNumero_de_tarjeta() {
        return numero_de_tarjeta;
    }

    public void setNumero_de_tarjeta(int numero_de_tarjeta) {
        this.numero_de_tarjeta = numero_de_tarjeta;
    }

    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public String getAño_caducidad() {
        return año_caducidad;
    }

    public void setAño_caducidad(String año_caducidad) {
        this.año_caducidad = año_caducidad;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getCvc() {
        return cvc;
    }

    public void setCvc(int cvc) {
        this.cvc = cvc;
    }

    @Override
    public String toString() {
        return "TTarjeta{" +
                "numero_de_tarjeta=" + numero_de_tarjeta +
                ", titular='" + titular + '\'' +
                ", mes=" + mes +
                ", año_caducidad='" + año_caducidad + '\'' +
                ", tipo='" + tipo + '\'' +
                ", cvc=" + cvc +
                '}';
    }
}
