package aplicacion;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class ConexionBDOO {
    private static final String BD_URL = "data.odb";

    private static EntityManager con = null;
    private static EntityManagerFactory emf;

    public static EntityManager getConexion() {
        if (con == null) {
            emf = Persistence.createEntityManagerFactory(BD_URL);
            con = emf.createEntityManager();
        }
        return con;
    }

    public static void cerrar() {
        if (con != null) {
            con.close();
            emf.close();
        }
    }

}