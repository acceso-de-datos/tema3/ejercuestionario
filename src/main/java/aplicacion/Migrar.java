package aplicacion;

import com.sun.tools.jconsole.JConsoleContext;

import javax.persistence.EntityManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Migrar {
    public static void allMigrate(Connection con, EntityManager em) throws SQLException {
        String consulta = "SELECT * FROM cliente c";
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(consulta);
        em.getTransaction().begin();
        while (rs.next()) {
            String direccion = rs.getString(3);

            direccion = direccion.substring(1, direccion.length() - 1);
            String[] datosDireccion = direccion.split(",");
            TDireccion direc = new TDireccion(datosDireccion[0],
                    Integer.parseInt(datosDireccion[1]),
                    datosDireccion[2],
                    datosDireccion[3],
                    datosDireccion[4]);

            String telefonos = rs.getString(4);

            telefonos = telefonos.substring(1, telefonos.length() - 1);
            String[] datosTelefonos = telefonos.split(",");

            ArrayList<String> TelefonosConcat = new ArrayList<String>();

            for (int i = 1; i < datosTelefonos.length; i++) {
                if (i == 1) {
                    TelefonosConcat.add(datosTelefonos[i].substring(2));
                } else if (i == datosTelefonos.length - 1) {

                    TelefonosConcat.add(datosTelefonos[i].substring(0, datosTelefonos[i].length() - 2));

                } else {
                    TelefonosConcat.add(datosTelefonos[i]);

                }
            }
            String[] arrayTelefonos = new String[TelefonosConcat.size()];
            for (int i = 0; i < arrayTelefonos.length; i++) {
                arrayTelefonos[i] = TelefonosConcat.get(i);
            }
            TListaTelefonos ListaTelef = new TListaTelefonos(datosTelefonos[0],
                    arrayTelefonos);

            String tarjetas = rs.getString(5);

            tarjetas = tarjetas.substring(1, tarjetas.length() - 1);
            String[] tarjetasList = tarjetas.split(",");
            TTarjeta tarj = new TTarjeta(Integer.parseInt(tarjetasList[0]),
                    tarjetasList[1],
                    Integer.parseInt(tarjetasList[2]),
                    tarjetasList[3],
                    tarjetasList[4],
                    Integer.parseInt(tarjetasList[5]));

            Cliente cli = new Cliente(rs.getString(1),
                    rs.getString(2),
                    direc,
                    ListaTelef,
                    tarj);

            em.persist(cli);

        }

        em.getTransaction().commit();
        rs.close();
        stmt.close();
        System.out.println("Datos migrados correctamente!");

    }
}
