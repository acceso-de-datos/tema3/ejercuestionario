package aplicacion;

import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Cliente {

    @Id @GeneratedValue
    private int cod;
    private String nombre;
    private String apellidos;
    private TDireccion direccion;
    private TListaTelefonos telefonos;
    private TTarjeta tarjeta;

    public Cliente( String nombre, String apellidos, TDireccion direccion, TListaTelefonos telefonos, TTarjeta tarjeta) {

        this.nombre = nombre;
        this.apellidos = apellidos;
        this.direccion = direccion;
        this.telefonos = telefonos;
        this.tarjeta = tarjeta;
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public TDireccion getDireccion() {
        return direccion;
    }

    public void setDireccion(TDireccion direccion) {
        this.direccion = direccion;
    }

    public TListaTelefonos getTelefonos() {
        return telefonos;
    }

    public void setTelefonos(TListaTelefonos telefonos) {
        this.telefonos = telefonos;
    }

    public TTarjeta getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(TTarjeta tarjeta) {
        this.tarjeta = tarjeta;
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "cod=" + cod +
                ", nombre='" + nombre + '\'' +
                ", apellidos='" + apellidos + '\'' +
                ", direccion=" + direccion +
                ", telefonos=" + telefonos +
                ", tarjeta=" + tarjeta +
                '}';
    }
}